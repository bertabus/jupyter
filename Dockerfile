# Start from a core stack version
FROM jupyter/datascience-notebook
# check if dns can get out
# RUN cat /etc/resolv.conf
# Install in the default python3 environment
# RUN pip install --upgrade pip
user root
RUN python -m pip install --upgrade pip
# for fpga design
RUN pip install myhdl
RUN pip install myhdlpeek
RUN pip install sphinx
# for geometric algebra
RUN pip install clifford
# Misc EE tools
RUN pip install scikit-rf
RUN pip install skidl
RUN pip install PySpice
RUN pip install PyNEC
# allows fun xkcd style plots
RUN pip install git+https://github.com/mkrphys/ipython-tikzmagic.git
# RUN pip install https://github.com/robjstan/tikzmagic.git
RUN pip install itikz
# add mito sheets
# RUN pip install mitosheet
RUN pip install ipywidgets
RUN conda install -y -c conda-forge nodejs
# RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager
RUN pip install jupyterlab_widgets
# RUN conda install -c conda-forge jupyter_contrib_nbextensions
RUN pip install jupyter_contrib_nbextensions
# RUN python -m mitosheet turnondataframebutton
RUN pip install --no-cache-dir mitosheet
# kicad libraries necessary for skidl to work properly
RUN mkdir -p /kicad/library
RUN mkdir -p /kicad/modules
RUN git clone https://gitlab.com/kicad/libraries/kicad-symbols /kicad/library
RUN git clone https://gitlab.com/kicad/libraries/kicad-footprints /kicad/modules
RUN apt-get update
RUN apt-get -y install gnupg
RUN echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | tee /etc/apt/sources.list.d/coral-edgetpu.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
RUN apt-get update
# Misc EE
RUN apt-get -y install graphviz
RUN apt-get -y install ngspice
# Font for xkcd graphs 
RUN apt-get -y install fonts-humor-sans
RUN apt-get -y install imagemagick
RUN apt-get -y install pdf2svg
RUN apt-get -y install libcairo2-dev libpango1.0-dev ffmpeg g++
RUN apt-get -y install texlive-latex-extra
RUN apt-get -y install texlive-extra-utils
RUN apt-get -y install dvisvgm
# install coral gpu tensorflow 
# RUN apt-get -y install gasket-dkms libedgetpu1-std
RUN apt-get install libedgetpu1-std
RUN mkdir -p /etc/udev/rules.d/
# this needs to match hosts response to "getent group|grep apex"
#RUN groupadd -g 1001 apex
RUN usermod -aG plugdev jovyan
RUN sh -c "echo 'SUBSYSTEM==\"usb\",ATTRS{idVendor}==\"1a6e\",GROUP=\"plugdev\"\nSUBSYSTEM==\"usb\",ATTRS{idVendor}==\"18d1\",GROUP=\"plugdev\"' >> /etc/udev/rules.d/99-tpu.rules"
RUN usermod -aG plugdev jovyan
# RUN conda install -y tensorflow-gpu cudatoolkit
RUN conda update -n base -c conda-forge conda
RUN conda install -c conda-forge cudatoolkit=11.2 cudnn=8.1.0
# also on host run this to allow nvidia access
# sudo apt install nvidia-modprobe
RUN python3 -m pip install tensorflow
# basemap is deprecated in favor of cartopy for geospatial data processing
RUN conda install -y cartopy
# install various helper libraries
RUN conda install -y lxml
RUN conda install -y html5lib
# install jupyter notebooks and myst parser
RUN conda install -y -c conda-forge myst-parser
RUN pip install myst-nb
RUN pip install sphinxcontrib-mermaid
RUN pip install sphinx-rtd-theme
RUN pip install linkify-it-py
# Clean up fonts
RUN rm ~/.cache/matplotlib -r
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
# RUN apt-get -y install python3-pycoral
# RUN pip show wheel
COPY tflite_runtime-2.5.0.post1-cp310-cp310-linux_x86_64.whl /tmp/tflite_runtime-2.5.0.post1-cp311-cp311-linux_x86_64.whl
RUN pip3 install /tmp/tflite_runtime-2.5.0.post1-cp311-cp311-linux_x86_64.whl
COPY pycoral-2.0.0-cp310-cp310-linux_x86_64.whl /tmp/pycoral-2.0.0-cp311-cp311-linux_x86_64.whl
RUN pip3 install /tmp/pycoral-2.0.0-cp311-cp311-linux_x86_64.whl
user jovyan
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/conda/lib
RUN pip install galgebra 
# RUN pip install --extra-index-url https://google-coral.github.io/py-repo/ pycoral
# RUN apt-get install pycoral
#RUN python2 -m pip install ipykernel
#RUN python2 -m ipykernel install --user
RUN pip install manim
RUN pip uninstall -y pycairo
RUN pip install pycairo==1.11.0
# skidl can get the library folder from searching the parent folder of KYSYSMOD for library folder
ENV KISYSMOD=/kicad/modules
RUN mkdir /home/jovyan/persist
RUN mkdir -p /home/jovyan/.jupyter/custom/
COPY custom.js /home/jovyan/.jupyter/custom/
RUN echo "**************finished build**************************"
