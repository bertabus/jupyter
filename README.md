# jupyter

## Rebuild Changelog

### rebuilt 2021 Nov 9

added mitosheet, rebuild for current libs.
Add gpu and tpu support, move to docker.io official since gitlab is slow.
Don't push in CI, push from local computer after building on local runner.

### 2020 OCT 20

Removed python2 runtime. Most things seem to be running still or better.
TIKZ appears to be broken.

### Rebuilt on Jan 6 2020

Rebuild for current libraries
